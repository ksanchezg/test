using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class CharacterThirdController : MonoBehaviour
{

    public Image imageMale;
    public Image imageFemale;
    public GameObject prevButton;
    public GameObject nextButton;
    private int index = 0;
    private int maxValue = 1;
    // Start is called before the first frame update
    void Start()
    {
        prevButton.SetActive(false);
        nextButton.SetActive(true);
        imageFemale.enabled = false;
        imageMale.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void nextImage()
    {
        imageFemale.enabled = true;
        imageMale.enabled = false;
        prevButton.SetActive(true);
        nextButton.SetActive(false);
    }

    public void prevImage()
    {
        imageFemale.enabled = false;
        imageMale.enabled = true;
        prevButton.SetActive(false);
        nextButton.SetActive(true);
    }

    public void continueLobby()
    {
        Debug.Log("Entre a loby");
        SceneManager.LoadScene(1);
    }
}

