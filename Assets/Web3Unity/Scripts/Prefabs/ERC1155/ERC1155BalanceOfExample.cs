using System.Collections;
using System.Numerics;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ERC1155BalanceOfExample : MonoBehaviour
{

    public GameObject cubeTest;
    public Text Prueba;
    async void Start()
    {
        string chain = "ethereum";
        string network = "rinkeby";
        //string contract = "0x495f947276749Ce646f68AC8c248420045cb7b5e";
        string contract = "0x88b48f654c30e99bc2e4a1559b4dcf1ad93fa656";
        string account = PlayerPrefs.GetString("Account");
        string tokenId = "67691974171052914917983932043134713969278025103107574261834761675268364959745";


        BigInteger balanceOf = await ERC1155.BalanceOf(chain, network, contract, account, tokenId);
        print(balanceOf);

        if (balanceOf > 0)
        {
            cubeTest.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            Prueba.text = "Si puedo acceder";
        }
        else
        {
            Prueba.text = "No puedo acceder";
        }

    }
    
}
