using BezierSolution;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaballoManager : MonoBehaviour
{
    [SerializeField] BezierWalkerWithSpeed moveCaballo;
    [SerializeField] Animator anim;
    public float speed;
    public float precio;
    public string nombreCaballo;
    public string nombreJinete;
    public string userId;
    public string racerId;
    public float position;
    public float status;
    public bool desacelerar=false;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void Update()
    {
        Estadisticas();

        if (desacelerar == true)
        {
            if (speed > 0)
                speed -= 0.2f;

            StartCoroutine(reducirMovimiento());
        }

        /*if (speed <= 0)
        {
            anim.SetBool("start", false);
            moveCaballo.enabled = false;
        }*/
    }

    public void Estadisticas()
    {
        moveCaballo.speed= speed;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "final")
        {
            desacelerar = true;
            Debug.Log("Termine la carrera");
        }
    }

    public IEnumerator reducirMovimiento()
    {
        yield return new WaitForSeconds(5);
        anim.SetBool("start", false);
        moveCaballo.enabled = false;
        desacelerar = false;
    }
}
