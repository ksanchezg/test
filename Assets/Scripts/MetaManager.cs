using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MetaManager : MonoBehaviour
{
    public List<string> ganadores;
    public static bool isFinal = false;
    public static string prizeFinal ="60.00";
    public string name;
    public float price;
    public float prize;
    public string place;
    public int participants;
    public string winner;
    public string nftWineer;
    public GameObject listaImagen;

    public Text[] listaCaballos;
    public GameObject[] textPosicion, franjas;
    // Start is called before the first frame update
    void Start()
    {
        ganadores = new List<string>();
        ListaInicial();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ListaInicial()
    {
        listaCaballos[0].text = DataLoadList.dataLoadList[0].horse_name +" - "+ DataLoadList.dataLoadList[0].user_name;
    }

    public void ListaFinal()
    {
        listaCaballos[0].text = DataLoadList.dataLoadList[0].horse_name + " - " + DataLoadList.dataLoadList[0].user_name;
        listaCaballos[1].text = ganadores[1].ToString();
        listaCaballos[2].text = ganadores[2].ToString();
        listaCaballos[3].text = ganadores[3].ToString();
        listaCaballos[4].text = ganadores[4].ToString();
        listaCaballos[5].text = ganadores[5].ToString();
        listaCaballos[6].text = ganadores[6].ToString();
        listaCaballos[7].text = ganadores[7].ToString();
        listaCaballos[8].text = ganadores[8].ToString();
        listaCaballos[9].text = ganadores[9].ToString();

        for(int i= 0; i < textPosicion.Length; i++)
        {
            textPosicion[i].SetActive(true);
        }
        for(int j= 0; j < franjas.Length; j++)
        {
            franjas[j].SetActive(true);
        }

        isFinal = true;

        StartCoroutine(listaOff());
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Caballo")
        {
            ganadores.Add(other.gameObject.GetComponent<CaballoManager>().nombreCaballo);
        }

        if (ganadores.Count == participants)
        {
            ListaFinal();
        }
        /*else
        {
            ListaInicial();
        }*/
    }

    public IEnumerator listaOff()
    {
        yield return new WaitForSeconds(3);
        listaImagen.SetActive(false);
    }
    
}
