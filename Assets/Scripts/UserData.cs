using Photon.Pun.Demo.Asteroids;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserData : MonoBehaviour
{
    public string[] estadisticas;
    public string nombreUser;
    string nombreScena;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Stats();

        nombreScena = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        if (nombreScena == "Autolobby"  || nombreScena=="InicioCarrera1")
        {
            GameObject photonLogin = GameObject.Find("MainPanel");
            photonLogin.GetComponent<LobbyMainPanel>().NombrePlayer = nombreUser;
        }

        
    }

    public void Stats()
    {
        nombreUser = SesionWeb.user.name;
        //Debug.Log(nombreCaballo);
    }
}
