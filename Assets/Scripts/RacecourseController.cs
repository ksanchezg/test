using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RacecourseController : MonoBehaviour
{
    public static bool isRaceRegister = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (MetaManager.isFinal)
        {
            MetaManager.isFinal = false;
            isRaceRegister = true;
            registrarCarrera();
     
           
        }
    }

    private static string URI_POST_STATS= "https://api-hipodromo.kevinsanchez.site/public/api/auth/stat/create";
    private static string URI_POST_RACE = "https://api-hipodromo.kevinsanchez.site/public/api/auth/race/create";
    private static string URI_POST_RACE_USER = "https://api-hipodromo.kevinsanchez.site/public/api/auth/raceuser/create";


    [System.Serializable]
    public struct Race
    {
        public string name;
        public string price;
        public string prize;
        public string place;
        public string participants;
        public string user_winner_id;
        public string nft_winner_id;
        public string id;

    }
   
    [System.Serializable]
    public struct RaceObject
    {
        public string message;
        public Race race;

    }
    public RaceObject raceObject;
    public void registrarCarrera()
    {
        StartCoroutine(postDataApiRegisterRace());
    }

    private IEnumerator postDataApiRegisterRace()
    {
        
        WWWForm form = new WWWForm();

        form.AddField("name", DataLoadList.dataLoadList[0].race_name);
        form.AddField("price", DataLoadList.dataLoadList[0].race_bet);
        form.AddField("prize", MetaManager.prizeFinal);
        form.AddField("place", DataLoadList.dataLoadList[0].race_place);
        form.AddField("participants", DataLoadList.dataLoadList.Count);
        form.AddField("user_winner_id", DataLoadList.dataLoadList[0].user_id);
        form.AddField("nft_winner_id", DataLoadList.dataLoadList[0].nft_id);

        Debug.LogWarning("race_name:"+DataLoadList.dataLoadList[0].race_name);
        Debug.LogWarning("race_bet:" + DataLoadList.dataLoadList[0].race_bet);
        Debug.LogWarning("prizeFinal:" + MetaManager.prizeFinal);
        Debug.LogWarning("race_place:" + DataLoadList.dataLoadList[0].race_place);
        Debug.LogWarning("participants:" + DataLoadList.dataLoadList.Count);
        Debug.LogWarning("user_winner_id:" + DataLoadList.dataLoadList[0].user_id);
        Debug.LogWarning("nft_winner_id:" + DataLoadList.dataLoadList[0].nft_id);

        //form.AddField("name", "asdas");
        //form.AddField("price", "12");
        //form.AddField("prize", "12");
        //form.AddField("place", "asd");
        //form.AddField("participants", "1");
        //form.AddField("user_winner_id", "1");
        //form.AddField("nft_winner_id", "1");


        byte[] rawData = form.data;

        string url = URI_POST_RACE;
        var web = new UnityWebRequest(url, "POST");
        web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
        web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");


        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {

            raceObject = JsonUtility.FromJson<RaceObject>(web.downloadHandler.text);
            Debug.LogWarning(web.downloadHandler.text);
            Debug.LogWarning("Se creo carrera id:"+ raceObject.race.id);
            registrarEstadisticas(raceObject.race.id);
            registrarRegistarJugadoresCarrera(raceObject.race.id);


        }
        else
        {
            Debug.LogWarning("Error en registrar carrera");
            Debug.LogWarning(web.downloadHandler.error);
        }
      

    }

    public void registrarEstadisticas(string race_id)
    {
        StartCoroutine(postDataApiRegisterStats(race_id));
    }

    private IEnumerator postDataApiRegisterStats(string race_id)
    {

        WWWForm form = new WWWForm();

        form.AddField("user_id", DataLoadList.dataLoadList[0].user_id);
        form.AddField("race_id", race_id);
        form.AddField("position", "1");
        form.AddField("status", "Winner");

        //form.AddField("user_id", "1");
        //form.AddField("race_id", race_id);
        //form.AddField("position", "1");
        //form.AddField("status", "Winner");


        byte[] rawData = form.data;

        string url = URI_POST_STATS;
        var web = new UnityWebRequest(url, "POST");
        web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
        web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");


        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {
            Debug.LogWarning(web.downloadHandler.text);
            Debug.LogWarning("Se creo estadisticas del jugador");


        }
        else
        {
            Debug.LogWarning("Error en registrar estadisticas");
        }

    }

    public void registrarRegistarJugadoresCarrera(string race_id)
    {
        StartCoroutine(postDataApiRegisterUserRace(race_id));
    }

    private IEnumerator postDataApiRegisterUserRace(string race_id)
    {

        WWWForm form = new WWWForm();

        form.AddField("race_id", race_id);
        form.AddField("user_id", DataLoadList.dataLoadList[0].user_id);

        Debug.LogWarning("race_name:" + race_id);
        Debug.LogWarning("race_bet:" + DataLoadList.dataLoadList[0].user_id);

        //form.AddField("race_id", race_id);
        //form.AddField("user_id", "1");

        byte[] rawData = form.data;

        string url = URI_POST_RACE_USER;
        var web = new UnityWebRequest(url, "POST");
        web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
        web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");


        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {
            Debug.LogWarning(web.downloadHandler.text);
            Debug.LogWarning("Se enlazo jugadores a la carrera!");


        }
        else
        {
            Debug.LogWarning("Error en registrar usuario_carrera");
        }

    }


}
