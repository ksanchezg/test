using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Race : MonoBehaviour
{
    public string id;
    public string race_id;
    public string race_name;
    public string race_price;
    public string race_prize;
    public string race_place;
    public string winner_name;

    public Race(string id, string raceId, string raceName, string racePrice, string racePrize, string racePlace, string winnerName)
    {
        this.id = id;
        race_id = raceId;
        race_name = raceName;
        race_price = racePrice;
        race_prize = racePrize;
        race_place = racePlace;
        winner_name = winnerName;
    }
}
