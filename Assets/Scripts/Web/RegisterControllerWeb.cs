using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class RegisterControllerWeb : MonoBehaviour
{


    [System.Serializable]
    public struct UserObject
    {
        public string name;
        public string username;
        public string password;
        public string password_confirmation;
        public string email;

    }
    public UserObject user;

    [System.Serializable]
    public struct Mensaje
    {
        public string message;
    }
    public Mensaje msg;

    [System.Serializable]
    public struct Error
    {
        public string [] name;
        public string [] username;
        public string [] password;
        public string [] password_confirmation;
        public string [] email;
    }
    public Error error;

    [SerializeField] TMP_InputField textName;
    [SerializeField] TMP_InputField textEmail;
    [SerializeField] TMP_InputField textUsername;
    [SerializeField] TMP_InputField textPasswordConfirmation;
    [SerializeField] TMP_InputField textPassword;

    public TMP_Text textRespuesta;

    public TMP_Text textRespName;
    public TMP_Text textRespEmail;
    public TMP_Text textRespUser;
    public TMP_Text textRespPass;
    //public TMP_Text textRespRepeat;

    public GameObject panelCarga;


    [ContextMenu("Registrarse")]
    public void Registrarse()
    {
        StartCoroutine(postDataApiRegister());
    }

    private IEnumerator postDataApiRegister()
    {
        panelCarga.SetActive(true);
        WWWForm form = new WWWForm();
        //form.AddField("email", user.email);
        //form.AddField("password", user.password);
        form.AddField("name", textName.text);
        form.AddField("username", textUsername.text);
        form.AddField("email", textEmail.text);
        form.AddField("password", textPassword.text);
        form.AddField("password_confirmation", textPasswordConfirmation.text);


        byte[] rawData = form.data;

        //string url = "http://127.0.0.1:8000/api/auth/login";
        string url = "https://api-metaverso.kevinsanchez.site/public/api/auth/register";
        var web = new UnityWebRequest(url, "POST");
        web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
        web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        //web.SetRequestHeader("Accept", "application/json");


        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {
            Debug.LogWarning(web.downloadHandler.text);
            msg = JsonUtility.FromJson<Mensaje>(web.downloadHandler.text);
            Debug.LogWarning(msg.message);

            textRespuesta.GetComponent<TMP_Text>().text = msg.message;
            //
            textRespName.GetComponent<TMP_Text>().text = "";
            textRespEmail.GetComponent<TMP_Text>().text = "";
            textRespUser.GetComponent<TMP_Text>().text = "";
            textRespPass.GetComponent<TMP_Text>().text = "";
            panelCarga.SetActive(false);
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(0);
        }
        else
        {
            //Debug.LogWarning("Error al solicitar datos");
            Debug.LogWarning("Error:" + web.error);
            Debug.LogWarning("Error:" + web.downloadHandler.text);
            error = JsonUtility.FromJson<Error>(web.downloadHandler.text);
            //Debug.LogWarning("Error User:" + error.password_confirmation[0]);
            if (error.name != null)
            {
                textRespName.GetComponent<TMP_Text>().text = error.name[0];
            }
            else
            {
                textRespName.GetComponent<TMP_Text>().text = "";
            }

            if (error.email != null)
            {
                textRespEmail.GetComponent<TMP_Text>().text = error.email[0];
            }
            else
            {
                textRespEmail.GetComponent<TMP_Text>().text = "";
            }

            if (error.username != null)
            {
                textRespUser.GetComponent<TMP_Text>().text = error.username[0];
            }
            else
            {
                textRespUser.GetComponent<TMP_Text>().text = "";
            }

            if (error.password != null)
            {
                textRespPass.GetComponent<TMP_Text>().text = error.password[0];
            }
            else
            {
                textRespPass.GetComponent<TMP_Text>().text = "";
            }




            //textRespRepeat.GetComponent<TMP_Text>().text = error.password_confirmation[0];
            //textRespuesta.GetComponent<TMP_Text>().text = "Some fields were incorrect. Please try again.";
            panelCarga.SetActive(false);
        }
        
    }

    public void goLogin()
    {
        SceneManager.LoadScene(0);
    }


}
