using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLoad : MonoBehaviour
{
    //user
    public string user_id;
    public string user_name;
    //nft
    public string nft_id;
    public string horse_id;
    public string horse_name;
    //race
    public string race_name;
    public string race_place;
    public string race_bet;

    public DataLoad(string userId, string userName, string nftId, string horseId, string horseName, string raceName, string racePlace, string raceBet)
    {
        user_id = userId;
        user_name = userName;
        nft_id = nftId;
        horse_id = horseId;
        horse_name = horseName;
        race_name = raceName;
        race_place = racePlace;
        race_bet = raceBet;
    }

    public DataLoad()
    {

    }
}
