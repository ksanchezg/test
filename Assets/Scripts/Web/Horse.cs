using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horse : MonoBehaviour
{
    public string id;
    public string user_name;
    public string nft_id;
    public string nft_tonken_id;
    public string nft_horse_id;
    public string horse_name;
    public string horse_description;
    public string rarity_name;
    public string velocity_name;
    public string genotype_name;
    public string bloodline_name;
    public string rent;
    public string price;
    public string first;
    public string second;
    public string third;

    public Horse(string id, string userName, string nftId, string nftToken, string horseId, 
                    string horseName, string horseDes, string rarity, string velocity, string genotype, 
                        string bloodline, string rent, string price, string first, string second, string third)
    {
        this.id = id;
        user_name = userName;
        nft_id = nftId;
        nft_tonken_id = nftToken;
        nft_horse_id = horseId;
        horse_name = horseName;
        horse_description = horseDes;
        rarity_name = rarity;
        velocity_name = velocity;
        genotype_name = genotype;
        bloodline_name = bloodline;
        this.rent = rent;
        this.price = price;
        this.first = first;
        this.second = second;
        this.third = third;
    }
}
