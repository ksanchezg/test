using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class LoginControllerWeb : MonoBehaviour
{


    //[System.Serializable]
    //public struct UserObject
    //{
    //    public string username;
    //    public string password;
    //    public string id;
    //
    //}
    //public UserObject user;

    [System.Serializable]
    public struct UserRespObject
    {
        public string id;
        public string name;
        public string username;
        public string email;
        public string wallet;
        public string points;

    }
    //public UserRespObject userResp;


    [System.Serializable]
    public struct SesionObject
    {
        public string access_token;
        public string token_type;
        public string expires_in;
        public UserRespObject user;

    }
    public SesionObject sesion;

    [SerializeField] TMP_InputField textUsername;
    [SerializeField] TMP_InputField textPassword;

    public TMP_Text textRespuesta;
    //public GameObject panelCarga;


    [ContextMenu("Iniciar Sesi�n")]
    public void iniciarSesion()
    {
        StartCoroutine(postDataApiLogin());
    }

    [ContextMenu("Cerrar Sesi�n")]
    public void cerarSesion()
    {
        //StartCoroutine(getDataApiLogout());
    }

    private IEnumerator postDataApiLogin()
    {
        //panelCarga.SetActive(true);
        WWWForm form = new WWWForm();
        //form.AddField("email", user.email);
        //form.AddField("password", user.password);
        form.AddField("username", textUsername.text);
        form.AddField("password", textPassword.text);

        byte[] rawData = form.data;

        string url = "https://api-hipodromo.kevinsanchez.site/public/api/auth/login";
       //string url = "https://api-metaverso.kevinsanchez.site/public/api/auth/login";
        var web = new UnityWebRequest(url, "POST");
        web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
        web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        //web.SetRequestHeader("Accept", "application/json");


        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {
            Debug.LogWarning(web.downloadHandler.text);

            sesion = JsonUtility.FromJson<SesionObject>(web.downloadHandler.text);

            SesionWeb.user.id = sesion.user.id;
            SesionWeb.user.name = sesion.user.name;
            SesionWeb.user.username = sesion.user.username;
            SesionWeb.user.email = sesion.user.email;
            SesionWeb.user.wallet = sesion.user.wallet;
            SesionWeb.user.points = sesion.user.points;
            SesionWeb.id = sesion.user.id;
            Debug.Log("sesion id: " + sesion.user.id);
            textRespuesta.GetComponent<TMP_Text>().text = "Success.";
            SceneManager.LoadScene(1);
            //asdasdsa

          

        }
        else
        {
            Debug.LogWarning("Error al solicitar datos");
            Debug.LogWarning("Error:" + web.error);
            textRespuesta.GetComponent<TMP_Text>().text = "Username or password was incorrect. Please try again.";

        }
        //panelCarga.SetActive(false);
    }

    //private IEnumerator getDataApiLogout()
    //{
    //    WWWForm form = new WWWForm();
    //    form.AddField("username", textUsername.text);
    //    form.AddField("password", textPassword.text);
    //
    //
    //    byte[] rawData = form.data;
    //    string url = "https://api-hipodromo.kevinsanchez.site/public/api/auth/logout";
    //    Debug.Log(sesion.access_token);
    //    var web = new UnityWebRequest(url, "POST");
    //    web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
    //    web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
    //    web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //
    //    web.SetRequestHeader("Authorization", "Bearer " + sesion.access_token);
    //    yield return web.SendWebRequest();
    //
    //    if (!web.isNetworkError && !web.isHttpError)
    //    {
    //        Debug.LogWarning(web.downloadHandler.text);
    //        SesionWeb.user = null;
    //
    //    }
    //    else
    //    {
    //        Debug.LogWarning("Error al solicitar datos");
    //    }
    //}


}
