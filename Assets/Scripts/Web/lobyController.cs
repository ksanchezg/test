using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.UI;

public class lobyController : MonoBehaviour
{
    public TMP_Text wallet;
    public TMP_Text points;
    static string URI_GET_HORSES = "https://api-hipodromo.kevinsanchez.site/public/api/auth/nftuser/";
    static string URI_GET_RACES = "https://api-hipodromo.kevinsanchez.site/public/api/auth/races";
    static string URI_GET_STATS = "https://api-hipodromo.kevinsanchez.site/public/api/auth/stat/";
    public TMP_Text resume;
    public TMP_Text resumeRace;

    // Start is called before the first frame update
    void Start()
    {
        //cargarCaballos();
       // cargarCarreras();
       // cargarEstadisticas();
        wallet.GetComponent<TMP_Text>().text = SesionWeb.user.wallet;
        points.GetComponent<TMP_Text>().text = SesionWeb.user.points;
        Debug.Log("wallet:" + SesionWeb.user.wallet);
        Debug.Log("points:" + SesionWeb.user.points);
        Debug.Log("sesion:" + SesionWeb.id);

        

    }

    // Update is called once per frame
    void Update()
    {
        if (StatsAction.isclick && StatsAction.action=='S')
        {
            resume.GetComponent<TMP_Text>().text = "Resume #"+statsList[StatsAction.index].race_id;
            winner.GetComponent<TMP_Text>().text = statsList[StatsAction.index].status;
            player.GetComponent<TMP_Text>().text = statsList[StatsAction.index].user_name;
            prize.GetComponent<TMP_Text>().text = "Prize: $" + statsList[StatsAction.index].race_prize;
            bet.GetComponent<TMP_Text>().text = "Bet: $" + statsList[StatsAction.index].race_price;
            place.GetComponent<TMP_Text>().text = statsList[StatsAction.index].race_place;
            position.GetComponent<TMP_Text>().text = statsList[StatsAction.index].position + "� Place";
            StatsAction.isclick = false;
        }else if (StatsAction.isclick && StatsAction.action == 'R')
        {
            resumeRace.GetComponent<TMP_Text>().text = "Resume #" + raceList[StatsAction.index].race_id;
            winnerRace.GetComponent<TMP_Text>().text = "Winner";
            playerRace.GetComponent<TMP_Text>().text = raceList[StatsAction.index].winner_name;
            prizeRace.GetComponent<TMP_Text>().text = "Prize: $" + raceList[StatsAction.index].race_prize;
            betRace.GetComponent<TMP_Text>().text = "Bet: $" + raceList[StatsAction.index].race_price;
            placeRace.GetComponent<TMP_Text>().text = raceList[StatsAction.index].race_place;
        }

        if (WalletController.isWalletUpdate)
        {
            WalletController.isWalletUpdate = false;
            wallet.GetComponent<TMP_Text>().text = SesionWeb.user.wallet;
            Debug.LogWarning("wallet text: "+ SesionWeb.user.wallet);
            
        }
    }

    [System.Serializable]
    public struct HorseObject
    {
        [System.Serializable]
        public struct Horse
        {
            public string id;
            public string user_name;
            public string nft_id;
            public string nft_tonken_id;
            public string nft_horse_id;
            public string horse_name;
            public string horse_description;
            public string rarity_name;
            public string velocity_name;
            public string genotype_name;
            public string bloodline_name;
            public string rent;
            public string price;
            public string first;
            public string second;
            public string third;

        }
        public Horse[] horse;

    }
    public HorseObject horses;

    List<Horse> horseList = new List<Horse>();

    public Text titleHorse;
    public Text nameHorse;
    public Text desHorse;
    public Text rarityHorse;
    public Text velocityHorse;
    public Text genotypeHorse;
    public Text bloodlineHorse;

    public void cargarCaballos()
    {
        StartCoroutine(getDataApiHorses());
    }

    private IEnumerator getDataApiHorses()
    {
        Debug.LogWarning("entre");
        UnityWebRequest web = UnityWebRequest.Get(URI_GET_HORSES+SesionWeb.user.id);
        Debug.LogWarning("next");
        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {
            Debug.LogWarning(web.downloadHandler.text);
            horses = JsonUtility.FromJson<HorseObject>("{\"horse\":" + web.downloadHandler.text + "}");

            if (horses.horse.Length > 0)
            {
                foreach (var val in horses.horse)
                {
                    horseList.Add(new Horse(val.id, val.user_name, val.nft_id, val.nft_tonken_id, val.nft_horse_id,val.horse_name, val.horse_description, val.rarity_name, val.velocity_name, val.genotype_name, val.bloodline_name, val.rent, val.price, val.first, val.second, val.third));
                }
            }
            Debug.LogWarning("sali");
            Debug.LogWarning("size horse: " + horseList.Count);
            addHorse();
            //loadHouses();
            //Debug.Log(item.id);
            //crearAsientos(filas,columnas);

        }
        else
        {
            Debug.LogWarning("Error al solicitar datos");
        }
    }


    [System.Serializable]
    public struct RaceObject
    {
        [System.Serializable]
        public struct Race
        {
            public string id;
            public string race_id;
            public string race_name;
            public string race_price;
            public string race_prize;
            public string race_place;
            public string winner_name;
        }
        public Race[] race;

    }
    public RaceObject races;

    List<Race> raceList;

    public TMP_Text winnerRace;
    public TMP_Text playerRace;
    public TMP_Text prizeRace;
    public TMP_Text betRace;
    public TMP_Text placeRace;
    public void cargarCarreras()
    {
        StartCoroutine(getDataApiRaces());
    }

    private IEnumerator getDataApiRaces()
    {
        raceList = new List<Race>();
        raceList.Clear();
        UnityWebRequest web = UnityWebRequest.Get(URI_GET_RACES);

        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {
            Debug.LogWarning(web.downloadHandler.text);
            races = JsonUtility.FromJson<RaceObject>("{\"race\":" + web.downloadHandler.text + "}");

            if (races.race.Length > 0)
            {
                int count = 0;
                foreach (var val in races.race)
                {
                    raceList.Add(new Race(val.id, val.race_id, val.race_name, val.race_price, val.race_prize, val.race_place, val.winner_name));
                    count++;
                    if (count > 6)
                        break;
                }
            }
            Debug.LogWarning("size race: "+raceList.Count);
            addRaces();
            //loadHouses();
            //Debug.Log(item.id);
            //crearAsientos(filas,columnas);

        }
        else
        {
            Debug.LogWarning("Error al solicitar datos");
        }
    }


    [System.Serializable]
    public struct StatObject
    {
        [System.Serializable]
        public struct Stat
        {
            public string id;
            public string user_id;
            public string user_name;
            public string user_username;
            public string race_id;
            public string race_name;
            public string race_price;
            public string race_prize;
            public string race_place;
            public string position;
            public string status;
        }
        public Stat[] stat;

    }
    public StatObject stats;

    List<Stats> statsList;
    public TMP_Text winner;
    public TMP_Text player;
    public TMP_Text prize;
    public TMP_Text bet;
    public TMP_Text place;
    public TMP_Text position;
    public void cargarEstadisticas()
    {
        StartCoroutine(getDataApiStats());
    }

    private IEnumerator getDataApiStats()
    {
        statsList = new List<Stats>();
        statsList.Clear();
        UnityWebRequest web = UnityWebRequest.Get(URI_GET_STATS + SesionWeb.user.id);

        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {
            Debug.LogWarning(web.downloadHandler.text);
            stats = JsonUtility.FromJson<StatObject>("{\"stat\":" + web.downloadHandler.text + "}");

            if (stats.stat.Length > 0)
            {
                int count=0;
                foreach (var val in stats.stat)
                {
                    statsList.Add(new Stats(val.id, val.user_id, val.user_name, val.user_username, val.race_id, val.race_name, val.race_price, val.race_prize, val.race_place, val.position, val.status));
                    count++;
                    if (count > 6)
                        break;
                }
            }
            Debug.LogWarning("size stats: " + statsList.Count);
            addStats();
            //loadHouses();
            //Debug.Log(item.id);
            //crearAsientos(filas,columnas);

        }
        else
        {
            Debug.LogWarning("Error al solicitar datos");
        }
    }

    public Transform listaEstadisticas;
    public GameObject plantillaEstadisticas;
    private void addStats()
    {
        foreach (Transform child in listaEstadisticas)
        {
            GameObject.Destroy(child.gameObject);
        }
        int index = 0;
        foreach (var val in statsList)
        {
            GameObject ints = Instantiate(plantillaEstadisticas, listaEstadisticas);
            ints.name =""+index;
            GameObject child1 = ints.transform.GetChild(0).gameObject;
            //GameObject child2 = ints.transform.GetChild(1).gameObject; #000001 - Racetrack A - 4st You($0.00)
            string amount="";
            if (val.position == "1")
                amount = val.race_prize;
            else
                amount = "0.00";
            child1.GetComponent<TMP_Text>().text = "#"+val.race_id+" "+val.race_name+" - "+"You($"+amount+") "+ val.position+"� Place";
            ints.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
            index++;
        }

        //initial
        resume.GetComponent<TMP_Text>().text = "Resume #" + statsList[0].race_id;
        winner.GetComponent<TMP_Text>().text = statsList[0].status;
        player.GetComponent<TMP_Text>().text = statsList[0].user_name;
        prize.GetComponent<TMP_Text>().text = "Prize: $"+statsList[0].race_prize;
        bet.GetComponent<TMP_Text>().text = "Bet: $" + statsList[0].race_price;
        place.GetComponent<TMP_Text>().text = statsList[0].race_place;
        position.GetComponent<TMP_Text>().text = statsList[0].position+"� Place";

    }


    public Transform listaCarreras;
    public GameObject plantillaCarreras;
    private void addRaces()
    {
        foreach (Transform child in listaCarreras)
        {
            GameObject.Destroy(child.gameObject);
        }
        int index = 0;
        foreach (var val in raceList)
        {
            GameObject ints = Instantiate(plantillaCarreras, listaCarreras);
            ints.name = "" + index;
            GameObject child1 = ints.transform.GetChild(0).gameObject;
            //GameObject child2 = ints.transform.GetChild(1).gameObject; #000001 - Racetrack A - 4st You($0.00)
        
          
            child1.GetComponent<TMP_Text>().text = "#" + val.race_id + " " + val.race_name + " - " + val.winner_name +"($" + val.race_prize + ") ";
            ints.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
            if (index == 5)
                break;
            index++;
            
        }

        //initial
        resumeRace.GetComponent<TMP_Text>().text = "Resume #" + raceList[0].race_id;
        winnerRace.GetComponent<TMP_Text>().text = "Winner";
        playerRace.GetComponent<TMP_Text>().text = raceList[0].winner_name;
        prizeRace.GetComponent<TMP_Text>().text = "Prize: $" + raceList[0].race_prize;
        betRace.GetComponent<TMP_Text>().text = "Bet: $" + raceList[0].race_price;
        placeRace.GetComponent<TMP_Text>().text = raceList[0].race_place;

    }

    public Transform listaCaballo;
    public GameObject plantillaCaballo;

    private void addHorse()
    {
        foreach (Transform child in listaCaballo)
        {
            GameObject.Destroy(child.gameObject);
        }

        int index = 0;
        foreach (var val in horseList)
        {
            GameObject ints = Instantiate(plantillaCaballo, listaCaballo);
            ints.name = "" + index;
            GameObject child1 = ints.transform.GetChild(0).gameObject;
            //GameObject child2 = ints.transform.GetChild(1).gameObject; #000001 - Racetrack A - 4st You($0.00)
            //child1.GetComponent<TMP_Text>().text = "#" + val.race_id + " " + val.race_name + " - " + val.winner_name + "($" + val.race_prize + ") ";
            ints.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
            if (index == 5)
                break;
            index++;
        }

        //initial
        titleHorse.GetComponent<Text>().text = horseList[0].bloodline_name;
        nameHorse.GetComponent<Text>().text = horseList[0].horse_name;
        desHorse.GetComponent<Text>().text = horseList[0].horse_description;
        rarityHorse.GetComponent<Text>().text = horseList[0].rarity_name;
        velocityHorse.GetComponent<Text>().text = horseList[0].velocity_name;
        genotypeHorse.GetComponent<Text>().text = horseList[0].genotype_name;
        bloodlineHorse.GetComponent<Text>().text = horseList[0].bloodline_name;

    }
    int horseIndex = 0;
    public void previusHorse()
    {
        horseIndex--;
        if (horseIndex < 0)
            horseIndex = 0;
        else if (horseIndex >= horseList.Count)
            horseIndex = horseList.Count - 1;
       
            titleHorse.GetComponent<Text>().text = horseList[horseIndex].bloodline_name;
            nameHorse.GetComponent<Text>().text = horseList[horseIndex].horse_name;
            desHorse.GetComponent<Text>().text = horseList[horseIndex].horse_description;
            rarityHorse.GetComponent<Text>().text = horseList[horseIndex].rarity_name;
            velocityHorse.GetComponent<Text>().text = horseList[horseIndex].velocity_name;
            genotypeHorse.GetComponent<Text>().text = horseList[horseIndex].genotype_name;
            bloodlineHorse.GetComponent<Text>().text = horseList[horseIndex].bloodline_name;
        
        
    }

    public void nextHorse()
    {
        horseIndex++;
        if (horseIndex < 0)
            horseIndex = 0;
        else if (horseIndex >= horseList.Count)
            horseIndex = horseList.Count - 1;

        titleHorse.GetComponent<Text>().text = horseList[horseIndex].bloodline_name;
            nameHorse.GetComponent<Text>().text = horseList[horseIndex].horse_name;
            desHorse.GetComponent<Text>().text = horseList[horseIndex].horse_description;
            rarityHorse.GetComponent<Text>().text = horseList[horseIndex].rarity_name;
            velocityHorse.GetComponent<Text>().text = horseList[horseIndex].velocity_name;
            genotypeHorse.GetComponent<Text>().text = horseList[horseIndex].genotype_name;
            bloodlineHorse.GetComponent<Text>().text = horseList[horseIndex].bloodline_name;
        
    }

    DataLoad dataLoad;

    public void saveRace()
    {
        Debug.LogWarning("Se guardo carrera");
        dataLoad = new DataLoad();
        dataLoad.race_name = "Race 1";
        dataLoad.race_place = "Place 1";
        dataLoad.race_bet = "10.00";
    }
    public void saveHorse()
    {
        Debug.LogWarning("Se guardo caballo");
        dataLoad.nft_id = horseList[horseIndex].nft_id;
        dataLoad.horse_id = horseList[horseIndex].nft_horse_id;
        dataLoad.horse_name = horseList[horseIndex].horse_name;
    }

    public void deleteHorse()
    {
        Debug.LogWarning("Se elimino caballo");
        dataLoad.nft_id = "";
        dataLoad.horse_id = "";
        dataLoad.horse_name = "";
    }

  
    public void loadData()
    {
        Debug.LogWarning("Se agrego datos a la lista para la carrera");
        dataLoad.user_id = SesionWeb.user.id;
        dataLoad.user_name = SesionWeb.user.name;
        DataLoadList.dataLoadList.Add(dataLoad);

        //string horse =DataLoadList.dataLoadList[0].horse_name;
    }

    


}
