using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.UI;

public class WalletController : MonoBehaviour
{
    public static bool isWalletUpdate = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (RacecourseController.isRaceRegister)
        {
            RacecourseController.isRaceRegister = false;
            anadirMontoWallet(MetaManager.prizeFinal);
            
        }
    }

    private static string URI_POST_WALLET_ADD = "https://api-hipodromo.kevinsanchez.site/public/api/auth/wallet/add/";
    private static string URI_POST_WALLET_SUBTRACT = "https://api-hipodromo.kevinsanchez.site/public/api/auth/wallet/subtract/";
    private static string URI_POST_POINT_ADD = "https://api-hipodromo.kevinsanchez.site/public/api/auth/point/add/";
    private static string URI_POST_POINT_SUBTRACT = "https://api-hipodromo.kevinsanchez.site/public/api/auth/point/subtract/";
    private static string URI_POST_WALLET_UPDATE = "https://api-hipodromo.kevinsanchez.site/public/api/auth/wallet/update/";
    


    [System.Serializable]
    public struct User
    {
        public string id;
        public string name;
        public string username;
        public string email;
        public string wallet;
        public string points;

    }

    [System.Serializable]
    public struct UserObject
    {
        public string message;
        public User user;

    }
    public UserObject userObject;

    public void anadirMontoWallet(string amount)
    {
        StartCoroutine(postDataApiAddWallet(amount));
    }

    private IEnumerator postDataApiAddWallet(string amount)
    {
        
        WWWForm form = new WWWForm();

        form.AddField("amount", amount);

        byte[] rawData = form.data;

        string url = URI_POST_WALLET_ADD+SesionWeb.user.id;
        var web = new UnityWebRequest(url, "POST");
        web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
        web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");


        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {

            userObject = JsonUtility.FromJson<UserObject>(web.downloadHandler.text);
            Debug.LogWarning(web.downloadHandler.text);
            SesionWeb.user.wallet = userObject.user.wallet;
            Debug.LogWarning("Se aumento wallet:" + userObject.user.wallet);
            isWalletUpdate = true;

        }
        else
        {
            Debug.LogWarning(web.downloadHandler.error);
        }


    }


    public void restarMontoWallet()
    {
        StartCoroutine(postDataApiSubtractWallet());
    }

    private IEnumerator postDataApiSubtractWallet()
    {
        
        WWWForm form = new WWWForm();

        form.AddField("amount", "10.00");

        byte[] rawData = form.data;

        string url = URI_POST_WALLET_SUBTRACT + SesionWeb.user.id;
        var web = new UnityWebRequest(url, "POST");
        web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
        web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");


        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {

            userObject = JsonUtility.FromJson<UserObject>(web.downloadHandler.text);
            Debug.LogWarning(web.downloadHandler.text);
            SesionWeb.user.wallet = userObject.user.wallet;
            Debug.LogWarning("Se resto wallet:" + userObject.user.wallet);
            isWalletUpdate = true;

        }


    }

    public void actualizarMontoWallet(TMP_Text amount)
    {
        StartCoroutine(postDataApiUpdateWallet(amount));
    }

    private IEnumerator postDataApiUpdateWallet(TMP_Text amount)
    {
        
        WWWForm form = new WWWForm();

        form.AddField("amount", amount.text);

        byte[] rawData = form.data;

        string url = URI_POST_WALLET_UPDATE + SesionWeb.user.id;
        var web = new UnityWebRequest(url, "POST");
        web.uploadHandler = (UploadHandler)new UploadHandlerRaw(rawData);
        web.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        web.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");


        yield return web.SendWebRequest();

        if (!web.isNetworkError && !web.isHttpError)
        {

            userObject = JsonUtility.FromJson<UserObject>(web.downloadHandler.text);
            Debug.LogWarning(web.downloadHandler.text);
            SesionWeb.user.wallet = userObject.user.wallet;
            Debug.LogWarning("Se actulizo wallet:" + userObject.user.wallet);
            isWalletUpdate = true;

        }
        else
        {
            Debug.LogWarning("error wallet:"+web.downloadHandler.error);
        }


    }

}
