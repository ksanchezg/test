using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public string id;
    public string user_id;
    public string user_name;
    public string user_username;
    public string race_id;
    public string race_name;
    public string race_price;
    public string race_prize;
    public string race_place;
    public string position;
    public string status;

    public Stats(string id, string userId, string userName, string userUsername, string raceId, string raceName, string racePrice, string racePrize, string racePlace, string position, string status)
    {
        this.id = id;
        user_id = userId;
        user_name = userName;
        user_username = userUsername;
        race_id = raceId;
        race_name = raceName;
        race_price = racePrice;
        race_prize = racePrize;
        race_place = racePlace;
        this.position = position;
        this.status = status;
    }
}
