using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll2 : MonoBehaviour
{
    //horses
    [SerializeField] GameObject scrollHorses;
    [SerializeField] int horsesSelect;
    //Rider

    //cambioDatos
    public Text nameHorse;

    public GameObject caballoImage;
    public Sprite Caballo1;
    public Sprite Caballo2;
    public Sprite Caballo3;
    public Sprite Caballo4;

   

    // Start is called before the first frame update
    void Start()
    {
        horsesSelect = 1;
      
    }

    // Update is called once per frame
    void Update()
    {
       
        switch (horsesSelect)
        {
            case 1:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(840, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo1;
                nameHorse.GetComponent<Text>().text = "HORSE A";

                break;
            case 2:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(640, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo2;
                nameHorse.GetComponent<Text>().text = "HORSE B";
                break;
            case 3:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(440, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo3;
                nameHorse.GetComponent<Text>().text = "HORSE C";
                break;
            case 4:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(240, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo4;
                nameHorse.GetComponent<Text>().text = "HORSE D";
                break;
            case 5:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(40, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo1;
                nameHorse.GetComponent<Text>().text = "HORSE E";
                break;
            case 6:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(-160, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo2;
                nameHorse.GetComponent<Text>().text = "HORSE F";
                break;
            case 7:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(-360, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo3;
                nameHorse.GetComponent<Text>().text = "HORSE G";
                break;
            case 8:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(-560, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo4;
                nameHorse.GetComponent<Text>().text = "HORSE H";
                break;
            case 9:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(-760, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo1;
                nameHorse.GetComponent<Text>().text = "HORSE I";
                break;
            default:
                break;
        }

    
        
        
    }
    public void FlechaIzqHorses()
    {
        if (horsesSelect > 1)
        {
            horsesSelect--;
        }
    }
    public void FlechaDerHorses()
    {
        if (horsesSelect < 9)
        {
            horsesSelect++;
        }

    }
    public void SelecHorseVer(int num)
    {
        horsesSelect = num;
    }
}
