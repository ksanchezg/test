using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour
{
    //horses
    [SerializeField] GameObject scrollHorses;
    [SerializeField] int horsesSelect;
    //Rider
    [SerializeField] GameObject scrollRider;
    [SerializeField] int riderSelect;
    //cambioDatos
    public Text nameHorse;

    public GameObject caballoImage;
    public Sprite Caballo1;
    public Sprite Caballo2;
    public Sprite Caballo3;
    public Sprite Caballo4;

    public Text nameJinete;
    public GameObject jineteImage;
    public Sprite jinete1;
    public Sprite jinete2;
    public Sprite jinete3;
    public Sprite jinete4;

    // Start is called before the first frame update
    void Start()
    {
        horsesSelect = 1;   
        riderSelect = 1;   
    }

    // Update is called once per frame
    void Update()
    {
        #region HorsesSelect
        switch (horsesSelect)
        {
            case 1:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(840, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo1;
                nameHorse.GetComponent<Text>().text = "HORSE A"  ;

                break;
            case 2:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(640, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo2;
                nameHorse.GetComponent<Text>().text = "HORSE B";
                break;
            case 3:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(440, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo3;
                nameHorse.GetComponent<Text>().text = "HORSE C";
                break;
            case 4:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(240, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo4;
                nameHorse.GetComponent<Text>().text = "HORSE D";
                break;
            case 5:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(40, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo1;
                nameHorse.GetComponent<Text>().text = "HORSE E";
                break;
            case 6:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(-160, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo2;
                nameHorse.GetComponent<Text>().text = "HORSE F";
                break;
            case 7:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(-360, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo3;
                nameHorse.GetComponent<Text>().text = "HORSE G";
                break;
            case 8:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(-560, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo4;
                nameHorse.GetComponent<Text>().text = "HORSE H";
                break;
            case 9:
                scrollHorses.GetComponent<RectTransform>().localPosition = new Vector3(-760, 0, 0);
                caballoImage.GetComponent<Image>().sprite = Caballo1;
                nameHorse.GetComponent<Text>().text = "HORSE I";
                break;
            default:
                break;
        }
        #endregion

        #region RiderSelect
        switch (riderSelect)
        {
            case 1:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(640, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete1;
                nameJinete.GetComponent<Text>().text = "NAKAMOTO";
                break;
            case 2:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(640, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete2;
                nameJinete.GetComponent<Text>().text = "MIYAMOTO";
                break;
            case 3:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(440, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete3;
                nameJinete.GetComponent<Text>().text = "CHINMUKI";
                break;
            case 4:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(240, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete4;
                nameJinete.GetComponent<Text>().text = "NARAZAKI";
                break;
            case 5:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(40, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete1;
                nameJinete.GetComponent<Text>().text = "MIRAJOKO";
                break;
            case 6:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(-160, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete2;
                nameJinete.GetComponent<Text>().text = "NAKAMOTO";
                break;
            case 7:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(-360, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete3;
                nameJinete.GetComponent<Text>().text = "MIRATOKA";
                break;
            case 8:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(-560, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete4;
                nameJinete.GetComponent<Text>().text = "MIRAZAKI";
                break;
            case 9:
                scrollRider.GetComponent<RectTransform>().localPosition = new Vector3(-760, 0, 0);
                jineteImage.GetComponent<Image>().sprite = jinete1;
                nameJinete.GetComponent<Text>().text = "NAGAMOTO";
                break;
            default:
                break;
        }
        #endregion
    }
    public void FlechaIzqHorses()
    {
            if (horsesSelect > 1)
            {
            horsesSelect --;
            }
    } 
   public void FlechaDerHorses()
   {
            if (horsesSelect <9)
            {
                horsesSelect++;
            }
            
   }  
    public void FlechaIzqRider()
    {
            if (riderSelect > 1)
            {
            riderSelect --;
            }
    } 
   public void FlechaDerRider()
   {
            if (riderSelect < 9)
            {
                riderSelect++;
            }
            
   }
}
