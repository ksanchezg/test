using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviourPunCallbacks
{
    private PhotonView photonView;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CargarEscena(string escena)
    {
        if (photonView.IsMine)
            StartCoroutine(desincronizar());
    }

    [PunRPC]
    public void rpcCambioEscena()
    {
        SceneManager.LoadScene("InicioCarrera1");
    }

    IEnumerator DisconnectAndLoad(string scene)
    {
        PhotonNetwork.LeaveRoom();
        yield return new WaitForSeconds(1);
        PhotonNetwork.AutomaticallySyncScene = false;
        while (PhotonNetwork.InRoom)
            yield return null;
        SceneManager.LoadScene(scene);
    }

    IEnumerator desincronizar()
    {
        PhotonNetwork.AutomaticallySyncScene = false;
        PhotonNetwork.LeaveRoom();
        Debug.Log(PhotonNetwork.AutomaticallySyncScene);
        yield return new WaitForSeconds(3);
        Debug.Log("Estoy desincronizando");
        PhotonNetwork.LoadLevel("InicioCarrera1");
        
    }


    #region Photon Callbacks


    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("Hipodromo");
    }


    #endregion


    #region Public Methods


    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }


    #endregion
}
