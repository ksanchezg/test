using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BezierSolution;

public class StartRace : MonoBehaviour
{
    public Timer timer;
    public GameObject goTittle;
    public BezierWalkerWithSpeed[] caballoMove;
    public GameObject listaCaballos;
    public UnityEngine.UI.Text tiempoText;
    bool goInicio = false;
    [SerializeField] private GameObject caballeriza;
    [SerializeField] private BoxCollider final;
    [SerializeField] private Animator[] animacionCaballo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject tiempo;
        GameObject jugador;
        jugador = GameObject.Find("TimerNoDestroy");
        //tiempo = jugador.transform.Find("Timer").gameObject;
        
        

        if (tiempoText.text=="00:10")
        {
             listaCaballos.SetActive(true);
            
        }

        if (tiempoText.text == "00:00")
        {
            for(int i = 0; i < caballoMove.Length; i++)
            {
                if (caballeriza.activeInHierarchy == true)
                {
                    caballoMove[i].enabled = true;
                    animacionCaballo[i].SetBool("start", true);
                }
            }

            goTittle.SetActive(true);
            StartCoroutine(CaballerizaOff());

        }
        else
        {
            tiempoText.text = jugador.GetComponent<Timer>().textoDelReloj;
        }
    }

    public IEnumerator go()
    {
        yield return new WaitForSeconds(1.5f);
        
        goInicio = true;
    }

    public IEnumerator CaballerizaOff()
    {
        yield return new WaitForSeconds(4);
        caballeriza.SetActive(false);
        final.enabled=true;
        yield return null;
    }

}
