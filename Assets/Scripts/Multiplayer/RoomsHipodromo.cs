using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Linq;

public class RoomsHipodromo : MonoBehaviourPunCallbacks
{
    /*[SerializeField] private byte maxPlayers=4;

    public void Start()
    {
        CreateRoom();
    }

    private void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = maxPlayers;
        PhotonNetwork.CreateRoom(null, roomOptions, null);
    }

    private void QuickMatch()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        CreateRoom();
    }

    public override void OnJoinedRoom()
    {
        // joined a room successfully
    }*/
    public UnityEngine.UI.Text status;
    public bool inicioSecion=false;
    bool startGame = false;
    public RoomOptions isVisible;
    public TypedLobby isOpen;
    public string[] maxNumberOfPlayers;

    public UnityEngine.UI.Text debu,debu2;
    private string roonName= "yooooo";

    public void Start()
    {
        //login();
        //StartCoroutine(starGame());
        //OnConnectedToMaster();
    }

    /*public void Update()
    {
       
        status.text = "" + PhotonNetwork.NetworkClientState;

        if (status.text == "ConnectedToMasterServer")
        {
            if (inicioSecion == false)
            {
                createRoom();
                inicioSecion = true;

                Debug.Log("cree la sala");
            }

        }


        if (inicioSecion == true)
        {
            if (startGame == false)
            {
                StartCoroutine(starGame());
                
            }
        }


        Debug.Log(PhotonNetwork.CountOfPlayersInRooms);
    }*/

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //createRoom();
            PhotonNetwork.JoinRoom(roonName);
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            createRoom();
            //PhotonNetwork.JoinRoom(roonName);
        }

        status.text = "" + PhotonNetwork.NetworkClientState;

        Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);


    }

    public void login()
    {
        string playerName = "albert";

        if (!playerName.Equals(""))
        {
            PhotonNetwork.LocalPlayer.NickName = playerName;
            PhotonNetwork.ConnectUsingSettings();
        }
        else
        {
            Debug.LogError("Player Name is invalid.");
        }
    }

    public void createRoom()
    {
        string roomName = "yooooo";
        roomName = (roomName.Equals(string.Empty)) ? "Room " + Random.Range(1000, 10000) : roomName;

        byte maxPlayers;
        byte.TryParse("4", out maxPlayers);
        maxPlayers = (byte)Mathf.Clamp(maxPlayers, 2, 8);

        RoomOptions options = new RoomOptions { MaxPlayers = maxPlayers, PlayerTtl = 10000 };

        PhotonNetwork.CreateRoom(roomName, options, null);
    }

    public IEnumerator starGame()
    {
        login();
        yield return new WaitForSeconds(5);
        //createRoom();
        
        yield return new WaitForSeconds(3);
        //PhotonNetwork.LoadLevel("Hipodromo");
    }

        

    public void OnPhotonJoinRoomFailed()
    {
        debu.text="OnPhotonRandomJoinFailed";
        PhotonNetwork.CreateRoom("yooooo", isVisible, isOpen, maxNumberOfPlayers);
    }

    public void OnCreatedRoom()
    {
        debu2.text="OnCreatedRoom";

        //set room properties
        //load level if needed, dont forget to disable messaging
    }

    public void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }
}
