using Photon.Pun;
using Photon.Pun.Demo.Asteroids;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rooms : MonoBehaviour
{
    [SerializeField] private LobbyMainPanel login;
    [SerializeField] private UnityEngine.UI.Text status;
    private bool empezar=false;
    private bool empezarScene = false;
    // Start is called before the first frame update
    void Start()
    {
        
        //login.OnLoginButtonClicked();
        //PhotonNetwork.ConnectToBestCloudServer();
        //string nombreScena=SceneManager.GetActiveScene().name;

        //if(nombreScena== "InicioCarrera1")

        StartCoroutine(startLogin());

    }

    // Update is called once per frame
    void Update()
    {
        status.text = "" + PhotonNetwork.NetworkClientState;
        if (status.text == "ConnectedToMasterServer")
        {
            if (empezar == false)
            {
                StartCoroutine(inicio());
                empezar = true;
                empezarScene = true;
            }

            if (empezarScene == true)
            {
                //StartCoroutine(startGame());
            }
        }

        
    }

    public void createdRoom()
    {
        RoomOptions options = new RoomOptions { MaxPlayers = 4, PlayerTtl = 10000 };

        PhotonNetwork.CreateRoom("carrera1", options, null);
    }
    public IEnumerator startLogin()
    {
        yield return new WaitForSeconds(1);
        //PhotonNetwork.LeaveRoom();
        yield return new WaitForSeconds(1);
        login.OnLoginButtonClicked();
    }

    public IEnumerator inicio()
    {
        yield return new WaitForSeconds(1);
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.JoinOrCreateRoom("carrera1", new RoomOptions() { MaxPlayers = 2 }, null);
        yield return new WaitForSeconds(3f);
        login.OnStartGameButtonClicked();
    }

    IEnumerator startGame()
    {
        yield return new WaitForSeconds(3f);
        login.StartGame();
        empezarScene = false;
    }
}
