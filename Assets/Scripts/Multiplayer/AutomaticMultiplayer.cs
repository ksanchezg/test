using Photon.Pun;
using Photon.Pun.Demo.Asteroids;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutomaticMultiplayer : MonoBehaviour
{
    [SerializeField] private LobbyMainPanel login;
    [SerializeField] private Text status;
    bool inicio=false;
    bool start = false;
    public Button startbutton;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(loginStart());
    }

    // Update is called once per frame
    void Update()
    {
        

        status.text = "" + PhotonNetwork.NetworkClientState;

        if (status.text == "ConnectedToMasterServer")
        {
            if (inicio == false)
            {
                login.OnJoinRandomRoomButtonClicked();
                inicio = true;
                start = true;
            }
            
            if(start == true)
            {
                //login.StartGame();
                //startbutton.Select();
                StartCoroutine(startGame());
            }
        }
    }

    IEnumerator autologin()
    {
        
        if(status.text == "ConnectedToMasterServer")
        login.OnJoinRandomRoomButtonClicked();
        Debug.Log("ConnectedToMasterServer");
        yield return new WaitForSeconds(1f);
        login.OnStartGameButtonClicked();
        //yield return null;
    }


    IEnumerator startGame()
    {
        yield return new WaitForSeconds(3f);
        login.StartGame();
        start = false;
    }

    IEnumerator loginStart()
    {
        yield return new WaitForSeconds(1f);
        login.OnLoginButtonClicked();
    }
}
