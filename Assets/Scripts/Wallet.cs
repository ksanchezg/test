using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Wallet : MonoBehaviour
{
    public double monedasWallet;
    private string TipoCarrera = "nada";
    private string RentaHorse = "Caballo1";
    public Text monedasWalletText;
    public Text monedasWalletText2;
    public Text monedasWalletText3;
    public Text monedasWalletText4;
    public TMP_Text cartera;
    //Walet
    public GameObject PanelTipoCarrera;
    public GameObject PanelCaballos;
    public GameObject PanelPopUpIrWallet;
    public GameObject PanelRider;
    public GameObject PanelRentaHorse;
    //renta
    public GameObject[] selectHorsesRenta;
    // Start is called before the first frame update
    void Start()
    {
        monedasWallet = double.Parse(SesionWeb.user.wallet);
    }

    // Update is called once per frame
    void Update()
    {
        monedasWalletText.text = monedasWallet.ToString(".00$");
        monedasWalletText2.text = monedasWallet.ToString(".00$");
        monedasWalletText3.text = monedasWallet.ToString(".00$");
        monedasWalletText4.text = monedasWallet.ToString(".00$");
        cartera.text = monedasWallet.ToString("0.00");

        //renta
        switch (RentaHorse)
        {
            case "Caballo1":
                selectHorsesRenta[0].SetActive(true);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo2":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(true);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo3":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(true);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo4":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(true);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo5":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(true);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo6":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(true);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo7":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(true);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo8":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(true);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo9":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(true);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo10":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(true);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo11":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(true);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo12":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(true);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo13":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(true);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo14":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(true);
                selectHorsesRenta[14].SetActive(false);
                break;
            case "Caballo15":
                selectHorsesRenta[0].SetActive(false);
                selectHorsesRenta[1].SetActive(false);
                selectHorsesRenta[2].SetActive(false);
                selectHorsesRenta[3].SetActive(false);
                selectHorsesRenta[4].SetActive(false);
                selectHorsesRenta[5].SetActive(false);
                selectHorsesRenta[6].SetActive(false);
                selectHorsesRenta[7].SetActive(false);
                selectHorsesRenta[8].SetActive(false);
                selectHorsesRenta[9].SetActive(false);
                selectHorsesRenta[10].SetActive(false);
                selectHorsesRenta[11].SetActive(false);
                selectHorsesRenta[12].SetActive(false);
                selectHorsesRenta[13].SetActive(false);
                selectHorsesRenta[14].SetActive(true);
                break;
            default:
                break;
        }
    }
    public void ListedGrade1()
    {
        if (monedasWallet >= 10)
        {
            monedasWallet -= 10;
            TipoCarrera = "ListedGrade1";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
            
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void ListedGrade2()
    {

        if (monedasWallet >= 25)
        {
            monedasWallet -= 25;
            TipoCarrera = "ListedGrade2";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
            
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void ListedGrade3()
    {
        if (monedasWallet >= 50)
        {
            monedasWallet -= 50;
            TipoCarrera = "ListedGrade3";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
           
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void ListedGrade4()
    {
        if (monedasWallet >= 100)
        {
            monedasWallet -= 100;
            TipoCarrera = "ListedGrade4";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
          
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void GroupGrade1()
    {
        if (monedasWallet >= 100)
        {
            monedasWallet -= 100;
            TipoCarrera = "GroupGrade1";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
           
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void GroupGrade2()
    {
        if (monedasWallet >= 250)
        {
            monedasWallet -= 250;
            TipoCarrera = "GroupGrade2";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
           
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void GroupGrade3()
    {
        if (monedasWallet >= 500)
        {
            monedasWallet -= 500;
            TipoCarrera = "GroupGrade3";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
           
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void GroupGrade4()
    {
        if (monedasWallet >= 1000)
        {
            monedasWallet -= 1000;
            TipoCarrera = "GroupGrade4";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
          
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void Group1()
    {
        if (monedasWallet >= 1000)
        {
            monedasWallet -= 1000;
            TipoCarrera = "Group1";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
           
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void Group2()
    {
        if (monedasWallet >= 2500)
        {
            monedasWallet -= 2500;
            TipoCarrera = "Group2";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
           
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void Group3()
    {
        if (monedasWallet >= 5000)
        {
            monedasWallet -= 5000;
            TipoCarrera = "Group3";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
           
            PanelPopUpIrWallet.SetActive(true);
        }
    }
    public void Group4()
    {
        if (monedasWallet >= 10000)
        {
            monedasWallet -= 10000;
            TipoCarrera = "Group4";
            PanelTipoCarrera.SetActive(false);
            PanelCaballos.SetActive(true);
        }
        else
        {
          
            PanelPopUpIrWallet.SetActive(true);
        }
    }

    public void RegresarCarrera()
    {
        switch (TipoCarrera)
        {
            case "ListedGrade1":
                monedasWallet += 10;
                break;
            case "ListedGrade2":
                monedasWallet += 25;
                break;
            case "ListedGrade3":
                monedasWallet += 50;
                break;
            case "ListedGrade4":
                monedasWallet += 100;
                break;
            case "GroupGrade1":
                monedasWallet += 100;
                break;
            case "GroupGrade2":
                monedasWallet += 250;
                break;
            case "GroupGrade3":
                monedasWallet += 500;
                break;
            case "GroupGrade4":
                monedasWallet += 1000;
                break;
            case "Group1":
                monedasWallet += 1000;
                break;
            case "Group2":
                monedasWallet += 2500;
                break;
            case "Group3":
                monedasWallet += 5000;
                break;
            case "Group4":
                monedasWallet += 10000;
                break;
            default:
                break;
        }
    }

    //renta caballo
    public void Renta1()
    {
        RentaHorse = "Caballo1";
    }
    public void Renta2()
    {
        RentaHorse = "Caballo2";
    }
    public void Renta3()
    {
        RentaHorse = "Caballo3";
    }
    public void Renta4()
    {
        RentaHorse = "Caballo4";
    }
    public void Renta5()
    {
        RentaHorse = "Caballo5";
    }
    public void Renta6()
    {
        RentaHorse = "Caballo6";
    }
    public void Renta7()
    {
        RentaHorse = "Caballo7";
    }
    public void Renta8()
    {
        RentaHorse = "Caballo8";
    }
    public void Renta9()
    {
        RentaHorse = "Caballo9";
    }
    public void Renta10()
    {
        RentaHorse = "Caballo10";
    }
    public void Renta11()
    {
        RentaHorse = "Caballo11";
    }
    public void Renta12()
    {
        RentaHorse = "Caballo12";
    }
    public void Renta13()
    {
        RentaHorse = "Caballo13";
    }
    public void Renta14()
    {
        RentaHorse = "Caballo14";
    }
    public void Renta15()
    {
        RentaHorse = "Caballo15";
    }
    public void NextRenta()
    {
        switch (RentaHorse)
        {
            case "Caballo1":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo2":
                if (monedasWallet >= 80)
                {
                    monedasWallet -= 80;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo3":
                if (monedasWallet >= 100)
                {
                    monedasWallet -= 100;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break; 
            case "Caballo4":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo5":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo6":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo7":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo8":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo9":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo10":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo11":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo12":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo13":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo14":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;
            case "Caballo15":
                if (monedasWallet >= 50)
                {
                    monedasWallet -= 50;                 
                    PanelRentaHorse.SetActive(false);
                    PanelRider.SetActive(true);
                }
                else
                {                   
                    PanelPopUpIrWallet.SetActive(true);
                }
                break;

            default:
                break;
        }
    }
}
