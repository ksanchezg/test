using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PanelPersonal : MonoBehaviour
{
    [SerializeField] private InputField fieldLink;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenEtherum()
    {
        Application.OpenURL("https://ethereum.org/en/");
    }

    public void CopyLink()
    {
        TextEditor textEditor = new TextEditor();
        textEditor.text = fieldLink.text;
        textEditor.SelectAll();
        textEditor.Copy();
    }
}


