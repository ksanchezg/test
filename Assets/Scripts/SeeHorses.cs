using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeHorses : MonoBehaviour
{
    //caballo
    [SerializeField] GameObject PanelCaballo;
    [SerializeField] GameObject PanelCaballoEstadisticasHorses;
    [SerializeField] GameObject MensajeCaballo;
    [SerializeField] bool EntroCaballo = false;
    //carrera
    [SerializeField] GameObject PanelCarrera;
    [SerializeField] GameObject MensajeCarrera;
    [SerializeField] bool EntroCarrera = false;
    //IniciarCarrera
    [SerializeField] GameObject PanelIniciarCarrera;
    [SerializeField] GameObject MensajeIniciarCarrera;
    [SerializeField] bool EntroIniciarCarrera = false;

    private PhotonView photonView;

    // Start is called before the first frame update
    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
       
    }
    private void FixedUpdate()
    {
        if (EntroCaballo && Input.GetKey(KeyCode.E))
        {
            PanelCaballo.SetActive(true);
            MensajeCaballo.SetActive(false);
        }
        if (EntroCarrera && Input.GetKey(KeyCode.E))
        {
            PanelCarrera.SetActive(true);
            MensajeCarrera.SetActive(false);
        }
        if (EntroIniciarCarrera && Input.GetKey(KeyCode.E))
        {
            PanelIniciarCarrera.SetActive(true);
            MensajeIniciarCarrera.SetActive(false);
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {

        if (photonView.IsMine)
        {
            if (other.gameObject.tag == "Caballo")
            {
                EntroCaballo = true;
                MensajeCaballo.SetActive(true);

            }
            if (other.gameObject.tag == "Carrera")
            {
                EntroCarrera = true;
                MensajeCarrera.SetActive(true);
            }
            if (other.gameObject.tag == "IniciarCarrera")
            {
                EntroIniciarCarrera = true;
                MensajeIniciarCarrera.SetActive(true);
            }
        }

        /*if (other.gameObject.tag == "Caballo")
        {     
            EntroCaballo = true;
            MensajeCaballo.SetActive(true);

        }
        if (other.gameObject.tag == "Carrera")
        {  
            EntroCarrera = true;
            MensajeCarrera.SetActive(true);
        } 
        if (other.gameObject.tag == "IniciarCarrera")
        {  
            EntroIniciarCarrera = true;
            MensajeIniciarCarrera.SetActive(true);
        }*/
    }
    private void OnTriggerExit(Collider other)
    {

        if (photonView.IsMine)
        {
            if (other.gameObject.tag == "Caballo")
            {
                PanelCaballo.SetActive(false);
                PanelCaballoEstadisticasHorses.SetActive(false);
                EntroCaballo = false;
                MensajeCaballo.SetActive(false);

            }
            if (other.gameObject.tag == "Carrera")
            {
                PanelCarrera.SetActive(false);
                EntroCarrera = false;
                MensajeCarrera.SetActive(false);
            }
            if (other.gameObject.tag == "IniciarCarrera")
            {
                PanelIniciarCarrera.SetActive(false);
                EntroIniciarCarrera = false;
                MensajeIniciarCarrera.SetActive(false);
            }
        }

        /*if (other.gameObject.tag == "Caballo")
        {   
            PanelCaballo.SetActive(false);
            PanelCaballoEstadisticasHorses.SetActive(false);
            EntroCaballo = false;
            MensajeCaballo.SetActive(false);

        }
        if (other.gameObject.tag == "Carrera")
        {
            PanelCarrera.SetActive(false);              
            EntroCarrera = false;
            MensajeCarrera.SetActive(false);
        } 
        if (other.gameObject.tag == "IniciarCarrera")
        {
            PanelIniciarCarrera.SetActive(false);              
            EntroIniciarCarrera = false;
            MensajeIniciarCarrera.SetActive(false);
        }*/
    }
}
